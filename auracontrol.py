import auraramcontroller
from auracontrollerbase import AuraController
import time
import pdb
import sys

AURA_CONTROLLER_RAM_ADDR = (0, 0x77)
# i2c addresses
# AURA_DEVICE_I2C_ADDR = 0x77

AURA_RAM_SLOT_LEFT1_ADDR = (0, 0x71)
AURA_RAM_SLOT_LEFT2_ADDR = (0, 0x74)
AURA_RAM_SLOT_LEFT3_ADDR = (0, 0x70)
AURA_RAM_SLOT_LEFT4_ADDR = (0, 0x73)

AURA_AUX_1_ADDR = (4, 0x4e)
AURA_AUX_2_ADDR = (4, 0x6f)

controllers = {}


def main():
    AURA_CONTROLLER_RAM = auraramcontroller.AuraRAMController(
        *AURA_CONTROLLER_RAM_ADDR, 'AuraControllerRam 0x77')
    AURA_RAM_SLOT_LEFT1 = auraramcontroller.AuraRAMController(
        *AURA_RAM_SLOT_LEFT1_ADDR, 'AuraRamSlotLeft1')
    AURA_RAM_SLOT_LEFT2 = auraramcontroller.AuraRAMController(
        *AURA_RAM_SLOT_LEFT2_ADDR, 'AuraRamSlotLeft2')
    AURA_RAM_SLOT_LEFT3 = auraramcontroller.AuraRAMController(
        *AURA_RAM_SLOT_LEFT3_ADDR, 'AuraRamSlotLeft3')
    AURA_RAM_SLOT_LEFT4 = auraramcontroller.AuraRAMController(
        *AURA_RAM_SLOT_LEFT4_ADDR, 'AuraRamSlotLeft4')
    global controllers
    controllers['AuraRAM'] = AURA_CONTROLLER_RAM
    controllers['AuraRAMSlots'] = [AURA_RAM_SLOT_LEFT1,
                                   AURA_RAM_SLOT_LEFT2, AURA_RAM_SLOT_LEFT3, AURA_RAM_SLOT_LEFT4]

    # AURA_AUX_1 = auraramcontroller.AuraRAMController(
    #     *AURA_AUX_1_ADDR, 'Aura Aux Unknown 1')
    # AURA_AUX_2 = auraramcontroller.AuraRAMController(
    #     *AURA_AUX_2_ADDR, 'Aura Aux Unknown 2')
    # controllers['AuraAUX'] = [AURA_AUX_1, AURA_AUX_2]

    print_mode_summary_for_devices()
    dump_aura_colors()
    set_static_color_auracontroller((255, 0, 0))
    # set_static_color_on_aux1((255, 0, 0))
    # set_static_color_ramslots((0, 255, 0))
    dump_aura_colors()


def set_static_color_on_aux1(rgb_tuple):
    print('-- Setting static color on AUX 1 --')
    controller = controllers['AuraAUX'][0]
    controller.set_effect_mode(
        AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'])
    controller.set_control_mode(AuraController.AURA_CONTROL_MODE_EFFECT)
    controller.set_rgb_data(AuraController.AURA_CONTROL_MODE_EFFECT,
                            [rgb_tuple for i in range(5)])
    controller.apply_changes()
    print('-- done --')

    # set_static_color_ramslots((0,255,0))
    # dump_aura_colors()
    # set_rainbow_ram()
    # dump_aura_colors()

    # set_mode_test()
    # pdb.set_trace()
    # set_static_color()


# def main():
#     if len(sys.argv) > 1:
#         # sys.argv[1] (parameter provided on cmdline) is the address to dump
#         print('Dumping', sys.argv[1])
#         dump_aura_device(4, int(sys.argv[1], 16))
#     else:
#         # print(flip_endian_16(0x8020))
#         AURA_DEVICE_I2C_ADDR = 0x77
#         dump_aura_device(0, AURA_DEVICE_I2C_ADDR)
#         # print(AuraRegisterRead(0x80f8))
#         # print(AuraRegisterRead(0x80f9))


# applies on each ram controller (after initing via 0x77 controller)
def set_rainbow_ram():
    print('Setting rainbow ram.')
    for controller in controllers['AuraRAMSlots']:
        controller.set_effect_mode(
            AuraController.AURA_EFFECT_MODES['AURA_MODE_RAINBOW'])
        controller.set_control_mode(AuraController.AURA_CONTROL_MODE_EFFECT)
        controller.apply_changes()

# applies on 0x77 controller


def set_rainbow_ram_auracontroller():
    print('Setting rainbow ram via 0x77 auraramcontroller')
    controllers['AuraRAM'].set_effect_mode(
        AuraController.AURA_EFFECT_MODES['AURA_MODE_RAINBOW'])
    controllers['AuraRAM'].set_control_mode(
        AuraController.AURA_CONTROL_MODE_EFFECT)
    controllers['AuraRAM'].apply_changes()

# example usage: set_static_color_ramslots((0,255,0))
# applies on each ram controller (after initing via 0x77 controller)


def set_static_color_ramslots(rgb_tuple):
    print('Setting staticcolor ram.')
    for controller in controllers['AuraRAMSlots']:
        controller.set_effect_mode(
            AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'])
        controller.set_control_mode(AuraController.AURA_CONTROL_MODE_EFFECT)
        controller.set_rgb_data(AuraController.AURA_CONTROL_MODE_EFFECT,
                                [rgb_tuple for i in range(5)])
        controller.apply_changes()

# example usage: set_static_color_auracontroller((0,255,0))
# applies on 0x77 controller


def set_static_color_auracontroller(rgb_tuple):
    print('Setting staticcolor ram via 0x77 auracontroller')
    controller = controllers['AuraRAM']
    controller.set_effect_mode(
        AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'])
    controller.set_control_mode(AuraController.AURA_CONTROL_MODE_EFFECT)
    controller.set_rgb_data(AuraController.AURA_CONTROL_MODE_EFFECT,
                            [rgb_tuple for i in range(5)])
    controller.apply_changes()


def hexstrip(int_val):
    return hex(int_val)[2:] if int_val is not None else 'XX'


def dump_ram_colors_directmode():
    print('RAMSlots Direct mode colors: ')
    for controller in controllers['AuraRAMSlots']:
        rgb_list = controller.get_rgb_data_by_mode(
            AuraController.AURA_CONTROL_MODE_DIRECT)
        print(controller, rgb_list)


def dump_ram_colors_effectmode():
    print('RAMSlots Effect mode colors: ')
    for controller in controllers['AuraRAMSlots']:
        rgb_list = controller.get_rgb_data_by_mode(
            AuraController.AURA_CONTROL_MODE_EFFECT)
        print(controller, rgb_list)


def dump_ram_colors_auracontroller():
    rgb_list = controllers['AuraRAM'].get_rgb_data_by_mode(
        AuraController.AURA_CONTROL_MODE_DIRECT)
    print('AuraController Direct mode colors: ', rgb_list)
    rgb_list = controllers['AuraRAM'].get_rgb_data_by_mode(
        AuraController.AURA_CONTROL_MODE_EFFECT)
    print('AuraController Effect mode colors: ', rgb_list)


def dump_aura_colors():
    print('------------ Dumping controller colors ------------')
    dump_ram_colors_auracontroller()
    dump_ram_colors_directmode()
    dump_ram_colors_effectmode()
    if 'AuraAUX' in controllers:
        dump_aux_devices_effectmode()
        dump_aux_devices_directmode()
    print('---------------------------------------------------')


def dump_aux_devices_effectmode():
    print('AuxDevices Effect mode colors: ')
    for controller in controllers['AuraAUX']:
        rgb_list = controller.get_rgb_data_by_mode(
            AuraController.AURA_CONTROL_MODE_EFFECT)
        print(controller, rgb_list)


def dump_aux_devices_directmode():
    print('AuxDevices Direct mode colors: ')
    for controller in controllers['AuraAUX']:
        rgb_list = controller.get_rgb_data_by_mode(
            AuraController.AURA_CONTROL_MODE_DIRECT)
        print(controller, rgb_list)


def dump_aura_colors_loop():
    while True:
        dump_aura_colors()
        time.sleep(1)


def print_mode_summary_for_devices():
    print('------------ Mode summary ------------')
    print(controllers['AuraRAM'], ' control_mode: ', controllers['AuraRAM'].get_control_mode())
    for controller in controllers['AuraRAMSlots']:
        print(controller, ' control_mode: ', controller.get_control_mode())
    if 'AuraAUX' in controllers:
        for controller in controllers['AuraAUX']:
            print(controller, ' control_mode: ', controller.get_control_mode())
    print('--------------------------------------')


def init_ram_i2c():
    print('------------ initing ram i2c ------------')
    # 0x70
    controllers['AuraRAM'].AuraRegisterWrite(0x80f8, 0x00)
    controllers['AuraRAM'].AuraRegisterWrite(0x80f9, 0xe0)

    # 0x71
    controllers['AuraRAM'].AuraRegisterWrite(0x80f8, 0x01)
    controllers['AuraRAM'].AuraRegisterWrite(0x80f9, 0xe2)

    # 0x73
    controllers['AuraRAM'].AuraRegisterWrite(0x80f8, 0x02)
    controllers['AuraRAM'].AuraRegisterWrite(0x80f9, 0xe6)

    # 0x74
    controllers['AuraRAM'].AuraRegisterWrite(0x80f8, 0x03)
    controllers['AuraRAM'].AuraRegisterWrite(0x80f9, 0xe8)
    print('------------ done ------------')

    """
    from https://gitlab.com/CalcProgrammer1/OpenAuraSDK/issues/3
    CreateAuraDevice(I2C_DRIVER_SMBUS_PIIX4, 0x0B00, 0x77);

        controllers[0]->AuraRegisterWrite(0x80f8, 0x00);
        controllers[0]->AuraRegisterWrite(0x80f9, 0xe0);

        controllers[0]->AuraRegisterWrite(0x80f8, 0x01);
        controllers[0]->AuraRegisterWrite(0x80f9, 0xe2);

        controllers[0]->AuraRegisterWrite(0x80f8, 0x02);
        controllers[0]->AuraRegisterWrite(0x80f9, 0xe6);

        controllers[0]->AuraRegisterWrite(0x80f8, 0x03);
        controllers[0]->AuraRegisterWrite(0x80f9, 0xe8);
    """


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == 'init':
            init_ram_i2c()
    main()
