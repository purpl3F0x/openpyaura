#!/usr/bin/env python3
import socket
import struct
import json
import threading
import traceback
import select

import jsonsocket

# thoughts
# a daemon will init multiple Controller() classes which have a simple set of attributes
# .get_led_count(), .set_rgb_list(), .get_rgb_list() etc
# already done in the auraramcontroller.py and aurausbcontroller.py
# which both write to local devices
# we'll add a AuraRemoteDevice(type) which will do the .write() to a socket
# and a AuraRemoteManager (below) to receive that. On the client side, just init a usb/ram controller object and write recved data
# AuraRemoteHost (below) will serve as initial control and do detection of controllable devices on the host
# connect to it to do .list_devices(), .init() blabla
# this allows the main lighting daemon to enumerate devices on a host (open a socket to a listening host)
# then make "dummy" network controllers which'll send to that host
# (each makes a separate connection, to diff ports (this is why the getting the list is needed))
###


class AuraNetworkHostManagerThread(object):
    thread = None
    clientsocket = None
    clientAddress = None
    manager = None
    stop_flag = False

    def __init__(self, clientsocket, clientAddress, manager):
        self.clientsocket = clientsocket
        self.clientAddress = clientAddress
        self.manager = manager

    def send(self, data):
        ret = jsonsocket.write_json(self.clientsocket, data)
        return ret

    def recv(self):
        return jsonsocket.read_json(self.clientsocket)

    def wrap_ok(self, resp):
        ret = {'status': 'ok', 'command': 'ack'}
        ret['data'] = resp
        return ret

    def wrap_fail(self, resp, err, tb_text):
        ret = {'status': 'error', 'error': str(
            err), 'command': 'ack', 'traceback': tb_text}
        ret['data'] = resp
        return ret

    def isShutdownMessage(self, jmsg):
        return jmsg['command'] == 'shutdown'

    def shutdown(self):
        self.stop_flag = True

    def serve(self):
        shutdown = False
        try:
            while True and not self.stop_flag:
                read_list = [self.clientsocket]
                # block for at most 3 seconds while checking if clientsocket has data
                readable, writable, errored = select.select(
                    read_list, [], [], 3)
                for s in readable:
                    jdata = jsonsocket.read_json(self.clientsocket)
                    if not self.isShutdownMessage(jdata):
                        # carry out action
                        try:
                            ret = self.act(jdata)
                            wrapped_ret = self.wrap_ok(ret)
                            self.send(wrapped_ret)
                        except Exception as err:
                            print('Exception in self.manager.act(): ', err)
                            print(type(err))
                            traceback.print_exc()
                            tb_text = traceback.format_exc()
                            ret = None
                            wrapped_ret = self.wrap_fail(ret, err, tb_text)
                            self.send(wrapped_ret)
                    else:
                        print('Got shutdown message from ', self.clientAddress)
                        shutdown = True
                        ret = 'ack_shutdown'
                        wrapped_ret = self.wrap_ok(ret)
                        self.send(wrapped_ret)
                        break
        except Exception as err:
            if not shutdown:
                print('Exception in AuraNetworkHostManagerThread: ', err)
                traceback.print_exc()
            else:
                pass
                # print('Exception because remote closed after shutdown, ignore.')
        finally:
            print('Exiting serve().')
            self.clientsocket.close()
            print('Ending AuraNetworkHostManagerThread thread -', self.clientAddress)

    def start(self):
        print('Starting AuraNetworkHostManagerThread with manager = ' +
              str(self.manager))
        self.thread = threading.Thread(target=self.serve)
        self.thread.start()

    def act(self, jmsg):
        print('Processing jmsg in AuraNetworkHostManagerThread: ', jmsg)
        return self.manager.act(jmsg)


class AuraNetworkHostManager(object):
    host = None
    port = None
    manager = None
    server_socket = None
    stop_flag = False
    rmts = []

    def __init__(self, manager, host, port):
        print('Initing AuraNetworkHostManager with manager:', manager)
        self.host = host
        self.port = port
        self.manager = manager
        # self.server_socket = socket.socket()
        # self.server_socket.bind((host, port))

        print('Starting AuraNetworkHostManager with manager = ' + str(self.manager))
        self.thread = threading.Thread(target=self.serve)
        self.thread.start()

    def shutdown(self):
        self.stop_flag = True
        print('Marking', len(self.rmts),
              'AuraNetworkHostManagerThread(s) for shutdown.')
        for rmt in self.rmts:
            rmt.shutdown()
        print('Marked ', str(self), ' for shutdown.')

    def get_status(self):
        return 'self.stop_flag=' + str(self.stop_flag) + '. clientthreads=' + str(len(self.rmts))

    def __str__(self):
        return 'AuraNetworkHostManager at ' + str(self.host) + ' ' + str(self.port)

    def serve(self):
        try:
            self.server_socket = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            # buffer_size = 4096
            self.server_socket.bind((self.host, self.port))
            self.server_socket.listen(10)
            read_list = [self.server_socket]
            print("Listening on %s:%s..." % (self.host, str(self.port)))
            while True and not self.stop_flag:
                # print('self.stop_flag in', self, self.stop_flag)
                # block for at most 3 seconds while checking for a readable server_socket
                readable, writable, errored = select.select(
                    read_list, [], [], 3)
                for s in readable:
                    client_socket, client_address = self.server_socket.accept()
                    print("Connection received from %s..." %
                          str(client_address))
                    rmt = AuraNetworkHostManagerThread(
                        client_socket, client_address, self.manager)
                    rmt.start()
                    self.rmts.append(rmt)
        except Exception as err:
            print('Exception in', str(self), err)
            traceback.print_exc()
        finally:
            self.server_socket.close()
            print('Ending ', str(self), 'thread.')
