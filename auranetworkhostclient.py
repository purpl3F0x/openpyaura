#!/usr/bin/env python3

# from auracontrollerbase import AuraController
from auraremotehostconnection import AuraRemoteConnection


class AuraNetworkHostClient(object):
    device = ''
    friendly_name = ''
    aura_remote_connector = None

    def __init__(self, manager, host, port):
        print('Initing AuraNetworkHostClient to', host, port)
        self.manager = manager
        self.friendly_name = 'AuraNetworkHostClient-' + str(manager)
        self.aura_remote_connection = AuraRemoteConnection(host, port)
        print('Initing AuraNetworkHostClient done. To', host, port)

    def connect(self):
        return self.aura_remote_connection.connect()

    def shutdown(self):
        print('Triggering shutdown of ', self)
        self.aura_remote_connection.shutdown()

    def __str__(self):
        return self.friendly_name

    # gotta send a shutdown_remote() command before this connection is closed
    # when this object is being deleted (going out of scope after a get_list, etc), to avoid a 
    # socket closed error on the other side.
    # def __del__(self):
    #     super.__del__()
    #     if self.aura_remote_connection:
    #         self.shutdown_remote()

    def send_to_remote(self, datadict):
        datadict['device'] = self.device
        datadict['friendlyname'] = self.friendly_name
        response = self.aura_remote_connection.send_to_remote(datadict)
        ret_data = None
        if response['status'] == 'ok':
            ret_data = response['data']
        return ret_data

    def get_remote_controller_details(self):
        # self.trigger_init_of_remote_controllers()
        ret = self.send_to_remote({'command': 'get_list_of_controllers'})
        print(ret)
        return ret

    def trigger_init_of_remote_controllers(self):
        ret = self.send_to_remote({'command': 'init_controllers'})
        print(ret)
        return ret

    def get_status(self):
        ret = self.send_to_remote({'command': 'get_status'})
        print(ret)
        return ret

    def shutdown_remote(self):
        ret = self.send_to_remote({'command': 'shutdown'})
        print(ret)
        return ret


# def main():
#     remotehost = AuraNetworkController('remotehost', '127.0.0.1', 8484)
#     remotehost.get_status()
#     remotehost.list_devices()
#     remotehost.shutdown_remote()
    # time.sleep(1)


# if __name__ == "__main__":
#     main()
