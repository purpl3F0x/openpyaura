from smbus2 import SMBus
import sys
import auracontrollerbase


def flip_endian_16(input_val: int):
    return (input_val << 8) & 0xff00 | (input_val >> 8) & 0x00ff


def hexstrip(int_val):
    return hex(int_val)[2:] if int_val is not None else 'XX'


class AuraRAMController(auracontrollerbase.AuraController):

    AURA_ADDR_START_RANGE = 0x8000
    AURA_ADDR_END_RANGE = 0x80ff

    AURA_BYTE_READ_ADDR = 0x81
    AURA_BYTE_WRITE_ADDR = 0x01

    # register addresses
    AURA_REG_COLORS_DIRECT = 0x8000
    AURA_REG_COLORS_EFFECT = 0x8010

    # the register storing the control mode (0x00 = direct, 0x01 = effect)
    AURA_REG_DIRECT = 0x8020

    # the register storing the effect mode(0-14, see auracontrollerbase.py)
    AURA_REG_MODE = 0x8021

    # the register to which you have to write AURA_REG_APPLY_VAL, to "apply changes"
    AURA_REG_APPLY = 0x80A0
    AURA_REG_APPLY_VAL = 0x01

    def __init__(self, bus_number, i2c_addr, friendly_name):
        self.bus_number = bus_number
        self.i2c_addr = i2c_addr
        self.smbus = SMBus(bus=bus_number)
        self.friendly_name = friendly_name

    def __repr__(self):
        return self.friendly_name + ' - AuraRAMController at smbus {} {:#x}'.format(self.bus_number, self.i2c_addr)

    def __str__(self):
        return self.__repr__()
        # return self.friendly_name

    def is_available(self):
        # is really a hacky is_this_an_aura_device() check
        # for now, try to read 0x00 on the bus and see if it returns ff
        # if ff, could be aura controller
        # if XX, no such device, so say no
        # if something else, could be RAM SPD or something, so say no.
        try:
            # print('Reading byte from address', iospace_reg)
            bdata = self.smbus.read_byte_data(
                self.i2c_addr, 0x00)
            if bdata == 0xff:
                return True
        except OSError:
            # Error 6: No such device or address => XX
            return False
        return False

    def AuraRegisterRead(self, register: int):
        # print('Register: '  + str(register))
        # print('Reading reg: ' + hex(flip_endian_16(register)))
        try:
            # print('Writing address', hex(register), 'to 0x00')
            self.smbus.write_word_data(self.i2c_addr, 0x00,
                                       flip_endian_16(register))
            # print('Reading byte from address', AURA_BYTE_READ_ADDR)
            bdata = self.smbus.read_byte_data(
                self.i2c_addr, self.AURA_BYTE_READ_ADDR)
            return bdata
        except OSError:
            # Error 6: No such device or address => XX
            return None

    def AuraRegisterWrite(self, register: int, value: int):
        self.smbus.write_word_data(self.i2c_addr, 0x00,
                                   flip_endian_16(register))
        self.smbus.write_byte_data(
            self.i2c_addr, self.AURA_BYTE_WRITE_ADDR, value)

    def AuraRegisterWriteBlock(self, register: int, data: list):
        self.smbus.write_word_data(self.i2c_addr, 0x00,
                                   flip_endian_16(register))
        self.smbus.write_i2c_block_data(self.i2c_addr, register, data)

    def dump_aura_device(self, start_range=AURA_ADDR_START_RANGE, end_range=AURA_ADDR_END_RANGE):
        print('Bus number:', self.bus_number, 'Device Address:', self.i2c_addr)
        print('      ' + ' '.join(["{0:#0{1}x}".format(i, 4)[2:]
                                   for i in range(16)]))
        print(hex(start_range)[2:] + ': ', end='', flush=True)
        for i in range(start_range, end_range + 1):
            # padded_i = "{0:#0{1}x}".format(i, 6)
            # print(padded_i + '=')
            b_val = self.AuraRegisterRead(i)
            if b_val is not None:
                display_val = "{0:#0{1}x}".format(b_val, 4)[2:]
                # display_val = hex(b_val)
            else:
                display_val = 'XX'
            print(display_val, end='', flush=True)
            print(' ', end='', flush=True)
            if (i+1) % 16 == 0 and i < self.AURA_ADDR_END_RANGE:
                print('\n' + hex(i+1)[2:] + ': ', end='', flush=True)
        print()

    # rgb_list is a list of 5 (R,B,G) tuples
    def _set_rgb_data(self, BASE_ADDRESS, rgb_list):
        while(len(rgb_list) < 5):
            rgb_list += rgb_list
        rgb_list = rgb_list[:5]
        # byte by byte:
        offset = 0
        for rgb in rgb_list:
            # for some reason, Aura RAM color regs store RBG instead of RGB.
            rbg = (rgb[0], rgb[2], rgb[1])
            for color in rbg:
                self.AuraRegisterWrite(BASE_ADDRESS + offset, color)
                offset += 1
        # block write mode (should be faster but isn't supported apparently?):
        # root@fowlmanor:~# i2cdetect -F 0
        # I2C Block Write                  no
        # flat_rgb_list = [item for sublist in rgb_list for item in sublist]
        # flat_rgb_list = [255 for i in range(15)]
        # interface.AuraRegisterWriteBlock(device, BASE_ADDRESS, flat_rgb_list)

    def set_rgb_data(self, control_mode, effect_mode, rgb_list, apply=True):
        # to maintain api compatibility with aurausbcontroller, set both data and modes here
        self.set_effect_mode(effect_mode)
        if control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT:
            self._set_rgb_data(self.AURA_REG_COLORS_EFFECT, rgb_list)
        elif control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            self._set_rgb_data(self.AURA_REG_COLORS_DIRECT, rgb_list)
        else:
            raise Exception('Unknown control mode: ' + str(control_mode))
        if apply:
            self.apply_changes()

    def get_rgb_data_by_mode(self, control_mode):
        print('Getting rgb data for control mode', control_mode, 'for', self)
        if control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT:
            return self._get_rgb_data(self.AURA_REG_COLORS_EFFECT)
        elif control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            return self._get_rgb_data(self.AURA_REG_COLORS_DIRECT)
        else:
            raise Exception('Unknown control_mode: ' + str(control_mode))

    def get_rgb_data(self):
        return self.get_rgb_data_by_mode(self.get_control_mode())

    def get_single_rgb_set(self, BASE_ADDRESS):
        # for some reason, Aura RAM color regs store RBG instead of RGB.
        R = hexstrip(self.AuraRegisterRead(BASE_ADDRESS))
        B = hexstrip(self.AuraRegisterRead(BASE_ADDRESS + 1))
        G = hexstrip(self.AuraRegisterRead(BASE_ADDRESS + 2))
        return (R, G, B)

    def _get_rgb_data(self, MODE_BASE_ADDRESS):
        rgb_sets = []
        for i in range(5):
            rgb_sets.append(self.get_single_rgb_set(MODE_BASE_ADDRESS + (i*3)))
        # print(rgb_sets)
        return rgb_sets

    def get_effect_mode(self):
        reg_mode = self.AuraRegisterRead(self.AURA_REG_MODE)
        for key, val in self.AURA_EFFECT_MODES.items():
            if val == reg_mode:
                print('RGB MODE: ' + key, val)
                return reg_mode
        print('Unknown RGB Mode. AURA_REG_MODE = ' + str(reg_mode))
        return reg_mode

    def get_control_mode(self):
        mode = self.AuraRegisterRead(self.AURA_REG_DIRECT)
        if mode is not None:
            if mode == self.AURA_CONTROL_MODE_EFFECT:
                print('Running in effect mode (0x8020=0)')
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT
            elif mode == self.AURA_CONTROL_MODE_DIRECT:
                print('Running in direct mode (0x8020=1)')
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT
            else:
                print('Unknown mode. Device ' + str(hex(self.i2c_addr)) +
                      ' - Mode (0x8020): ' + str(mode))
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_UNKNOWN
        else:
            print('Unknown mode. Device ' + str(hex(self.i2c_addr)) +
                  ' may be unavailable. 0x8020: ' + str(mode))
            return auracontrollerbase.AuraController.AURA_CONTROL_MODE_UNKNOWN

    def set_control_mode(self, mode, apply=True):
        # this is direct vs effect mode. ram: direct/effect = 0/1. usb: 3b/40. so gotta lookup
        if mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            self.AuraRegisterWrite(self.AURA_REG_DIRECT, self.AURA_CONTROL_MODE_DIRECT)
        else:
            self.AuraRegisterWrite(self.AURA_REG_DIRECT, self.AURA_CONTROL_MODE_EFFECT)
        if apply:
            self.apply_changes()

    def set_effect_mode(self, mode, apply=True):
        # this is one of the 14 modes in auracontrollerbase.py
        # don't need to change this since the values are the same for usb and ram
        self.AuraRegisterWrite(self.AURA_REG_MODE, mode)
        if apply:
            self.apply_changes()

    def apply_changes(self):
        self.AuraRegisterWrite(self.AURA_REG_APPLY, self.AURA_REG_APPLY_VAL)


def debug_dump():
    if len(sys.argv) > 2:
        bus_number = int(sys.argv[1])
        device = int(sys.argv[2], 16)
        friendly_name = 'aura device debugging'
        controller = AuraRAMController(bus_number, device, friendly_name)
        controller.dump_aura_device()
        return 0
    else:
        print('Library for interfacing with i2c aura devices. Running this as a file, provide the following for debug:')
        print('Usage: python3 auracontroller.py')
        return 1


def detect_and_init_devices():
    # real_detect - expose this better
    real_detect = True
    if real_detect:
        return real_detect_and_init_devices()
    else:
        return fake_detect_and_init_devices()

def real_detect_and_init_devices():
    controllers = []
    for busnum in range(0, 5):
        try:
            for i2c_addr in range(0x60, 0x77 + 1):
                c = AuraRAMController(
                    busnum, i2c_addr, 'AuraControllerRam Generic')
                if c.is_available():
                    controllers.append(c)
        except OSError as err:
            print('smbus number', busnum,
                  'not found. can\'t controllers on this (eg. aux aura).', err)
            break
    return controllers


def fake_detect_and_init_devices():
    # hardcoded for now, need to autodetect

    # (bus number, i2c address)
    AURA_CONTROLLER_RAM_ADDR = (0, 0x77)

    AURA_RAM_SLOT_LEFT1_ADDR = (0, 0x71)
    AURA_RAM_SLOT_LEFT2_ADDR = (0, 0x74)
    AURA_RAM_SLOT_LEFT3_ADDR = (0, 0x70)
    AURA_RAM_SLOT_LEFT4_ADDR = (0, 0x73)

    AURA_AUX_1_ADDR = (4, 0x4e)
    AURA_AUX_2_ADDR = (4, 0x6f)

    AURA_CONTROLLER_RAM = AuraRAMController(
        *AURA_CONTROLLER_RAM_ADDR, 'AuraControllerRam 0x77')
    AURA_RAM_SLOT_LEFT1 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT1_ADDR, 'AuraRamSlotLeft1')
    AURA_RAM_SLOT_LEFT2 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT2_ADDR, 'AuraRamSlotLeft2')
    AURA_RAM_SLOT_LEFT3 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT3_ADDR, 'AuraRamSlotLeft3')
    AURA_RAM_SLOT_LEFT4 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT4_ADDR, 'AuraRamSlotLeft4')
    controllers = {}
    controllers['AuraRAM'] = [AURA_CONTROLLER_RAM]
    controllers['AuraRAMSlots'] = [AURA_RAM_SLOT_LEFT1,
                                   AURA_RAM_SLOT_LEFT2, AURA_RAM_SLOT_LEFT3, AURA_RAM_SLOT_LEFT4]

    try:
        AURA_AUX_1 = AuraRAMController(
            *AURA_AUX_1_ADDR, 'Aura Aux Unknown 1')
        AURA_AUX_2 = AuraRAMController(
            *AURA_AUX_2_ADDR, 'Aura Aux Unknown 2')
        controllers['AuraAUX'] = [AURA_AUX_1, AURA_AUX_2]
    except OSError as err:
        print('aux smbus not found. can\'t init aux aura controller.', err)

    return [controller for controller in ([controllers['AuraRAM']] + controllers['AuraRAMSlots']) if controller.is_available()]


if __name__ == "__main__":
    cs = real_detect_and_init_devices()
    print(cs)
    for c in cs:
        c.dump_aura_device()
    # exit(debug_dump())
