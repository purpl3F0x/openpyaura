# OpenPyAURA

This project aims to provide an interface to control ASUS Aura compatible RGB devices (RAM, Addressable RGB Strips, etc), and an all-in-one lighting service daemon to control such devices across local and network hosts.

In progress, at early stages so far.

Please check the wiki/issues and have a look at this project as well, we're working side by side to understand the protocols and controllers
https://gitlab.com/CalcProgrammer1/OpenAuraSDK


Current state:

- RGB support:
    - Aura RAM control - detection and use of individual sticks, or combined Aura controller (only Linux)
        - Can set arbitrary RGB points and inbuilt modes
    - Aura Addressable RGB (ARGB) header controller support (Linux and Windows)
        - Can set arbitrary RGB points and inbuilt modes
    - Partial - Motherboard RGB zones and RGB headers (non-addressable) - on the Asus Crosshair VII X470 AMD and a few other boards, these devices are on an auxiliary smbus, which needs a kernel patch to detect
- Lighting Service Daemon:
    - RGB functionality:
        - Detects support RGB hardware (see above), initializes controller classes to use
        - Able to control networked RGB devices (devices on a host running the same daemon) and local RGB devices in a uniform "API"
    - Network functionality
        - Daemon master service connects to listed agent daemons, which expose their RGB devices over the network, for the master daemon to control.
        - Commands are JSON over TCP, to multithreaded network hosts.
        - An agent daemon can have any number of clients connected to any of its detected RGB devices - each RGB device receives its own commands
    - Dummy effect setting thread to set one-off or repeated color patterns

To Do:

- More digging into ASUS Aura motherboard RGB zones control
- Make a "reference" GUI app to configure the daemon service and provide commands.
- Add a generalized RGB device class so non-Aura devices can also be controlled (locally and over the network)
- Provide music reactive effects, color sync effects.
- Add "Basic" and "Advanced" modes to the lighting daemon, to allow setting simple internal effects, as well as per-led/per-zone effects taking into account relative placement of LEDs and devices, locally and across the network! All effects will work across all local and networked RGB devices!

Requirements:

- Python 3
- For RAM control:
    - Aura RAM works over the i2c bus - on linux, your board should be supported by the provided i2c drivers
    - For Windows, not supported yet. (Though you could just use the official Aura Control Center for inbuilt effects)
- For Addressable RGB (ARGB) strips:
    - Aura ARGB headers work over a USB HID device. For now, this project uses libusb on both Windows and Linux.
        - For linux: Install the python3 pip module `pyusb`. libusb should work out of the box.
        - For Windows: Download the built libusb DLLs from: https://libusb.info/ and add it to your System32/SysWOW64 path. 
            - MS64/dll/libusb-1.0.dll in Windows/System32, and MS32/dll/libusb-1.0.dll in Windows/SysWOW64 (depending on whether you want to use 32bit or 64bit python, or just add both)


Devices:
Working to incorporate all possible Aurasync devices and beyond

- Tested with:
    - Motherboards:
        - Asus Crosshair VII X470 AMD
            - ARGB headers (linux and windows)
            - motherboard RGB zones need a kernel patch on Linux (only linux for now)
    - RAM (Only linux for now)
        - ADATA XPG Spectrix D40 RAM


Usage:

If you have a single machine with the RGB devices you want to control, no need to run agent daemons.

To run the main daemon:

```
python3 auradaemon.py
```

NOTE: all code is in early development and may not make sense or have great documentation :p
Am actively working on this and welcome contributions and discussion!
