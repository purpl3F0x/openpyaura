# import abc

class AuraController(object):
    friendly_name = 'GenericAuraController'

    # self defined
    AURA_CONTROL_MODE_EFFECT = 0x0
    AURA_CONTROL_MODE_DIRECT = 0x1
    AURA_CONTROL_MODE_UNKNOWN = -0x1

    AURA_EFFECT_MODE_UNKNOWN = -0x1

    AURA_EFFECT_MODES = {
        'AURA_MODE_OFF': 0x0,
        'AURA_MODE_STATIC': 0x1,
        'AURA_MODE_BREATHING': 0x2,
        'AURA_MODE_FLASHING': 0x3,
        'AURA_MODE_SPECTRUM_CYCLE': 0x4,
        'AURA_MODE_RAINBOW': 0x5,
        'AURA_MODE_SPECTRUM_CYCLE_BREATHING': 0x6,
        'AURA_MODE_CHASE_FADE': 0x7,
        'AURA_MODE_SPECTRUM_CYCLE_CHASE_FADE': 0x8,
        'AURA_MODE_CHASE': 0x9,
        'AURA_MODE_SPECTRUM_CYCLE_CHASE': 0x10,
        'AURA_MODE_SPECTRUM_CYCLE_WAVE': 0x11,
        'AURA_MODE_CHASE_RAINBOW_PULSE': 0x12,
        'AURA_MODE_RANDOM_FLICKER': 0x13,
        'AURA_MODE_MUSIC': 0x14,
    }
    # def __init__(self, friendly_name):
    #     self.bus_number = bus_number
    #     self.friendly_name = friendly_name

    def __str__(self):
        return self.friendly_name

    # @abc.ABCMeta.abstractmethod
    # def test_method():
    #     pass

    # @abc.ABCMeta.abstractmethod
    def dump_aura_device(self):
        raise NotImplementedError('dump_aura_device not implemented in ', self)

    def get_rgb_data(self, mode):
        raise NotImplementedError('get_rgb_data not implemented in ', self)

    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        """
        rgb_list is list of (R,B,G) tuples of hex/int
        """
        raise NotImplementedError('set_rgb_data not implemented in ', self)

    def get_effect_mode(self):
        raise NotImplementedError('dump_aurget_effect_modea_device not implemented in ', self)

    def get_control_mode(self):
        raise NotImplementedError('get_control_mode not implemented in ', self)

    def set_control_mode(self, mode):
        raise NotImplementedError('set_control_mode not implemented in ', self)

    def set_effect_mode(self, mode):
        raise NotImplementedError('set_effect_mode not implemented in ', self)

    def apply_changes(self):
        raise NotImplementedError('apply_changes not implemented in ', self)
