#!/usr/bin/env python3
import time
import threading

import auraramcontroller
import aurausbcontroller
import auranetworkcontroller
from auracontrollerbase import AuraController
import utils
from auranetworkhostclient import AuraNetworkHostClient
from auranetworkhostmanagerbase import AuraNetworkHostManager
from auracommandsink import AuraCommandSink

remote_controllers_config = [
    {
        'host': '10.0.0.41',
        'port': 8484
    }
]


class AuraDaemon(AuraCommandSink):
    host = None
    port = None
    ram_controllers = []
    usb_controllers = []
    remote_controllers = []

    aura_network_host = None
    remote_host_clients = []
    init_done = False
    stop_flag = False

    def __init__(self, host='0.0.0.0', port=8484):
        print('Initing master Aura daemon.')
        self.host = host
        self.port = port
        # init local controllers
        self.ram_controllers = auraramcontroller.detect_and_init_devices()
        self.usb_controllers = aurausbcontroller.detect_and_init_devices()

        # init self server socket to accept command connections to this daemon
        self._init_self_network_host()

        # according to the config, set up connections to manage aura network hosts (which host controllers on them)

        self.thread = threading.Thread(
            target=self._init_remote_network_connection_clients)
        self.thread.start()

        print('Init done.')
        print('RAM controllers:', self.ram_controllers)
        print('USB controllers:', self.usb_controllers)
        print('Remote controllers so far:', self.remote_controllers)
        self.init_done = True

    def _init_self_network_host(self):
        self.aura_network_host = AuraNetworkHostManager(
            self, self.host, self.port)

    def _init_remote_network_connection_clients(self):
        for remote_host in remote_controllers_config:
            print('Trying to contact', remote_host, 'for remote controllers')
            remote_host_client = AuraNetworkHostClient(
                self, remote_host['host'], remote_host['port'])
            self.remote_host_clients.append(remote_host_client)
            print('New remote host added: ', remote_host_client)
            # print('In auradaemon, trying to connect() created networkhostclient.')
            status = remote_host_client.connect()
            # print('status:', status)
            if status and not self.stop_flag:
                remote_controller_details = remote_host_client.get_remote_controller_details()
                for remote_controller_detail in remote_controller_details:
                    remote_controller = auranetworkcontroller.AuraNetworkController(
                        remote_controller_detail['controller_name'], remote_controller_detail['host'], remote_controller_detail['port'])
                    status = remote_controller.connect()
                    self.remote_controllers.append(remote_controller)

    def launch_effect_threads(self):
        self.thread = threading.Thread(target=self.test_effect_func)
        self.thread.start()

    def run(self):
        self.launch_effect_threads()
        while True:
            try:
                # print('event loop hmm')
                # print('Remote host clients so far:', self.remote_host_clients)
                # print('Remote controllers so far:', self.remote_controllers)
                time.sleep(3)
            except KeyboardInterrupt as err:
                self.stop_flag = True
                print('Caught KeyboardInterrupt, triggering shutdown of remote host clients.')
                [f.shutdown() for f in self.remote_host_clients]
                print(
                    'From auranetworkcontrollerhost - triggering shutdown of NetworkHostManager.')
                self.aura_network_host.shutdown()
                break
        print('Exiting AuraDaemon in 4 seconds.')
        time.sleep(4)

    def test_effect_func(self):
        time.sleep(1)
        print('starting test effect')
        try:
            while True and not self.stop_flag:
                for color in [utils.red, utils.green, utils.blue]:
                    for controller in self.ram_controllers + self.usb_controllers + self.remote_controllers:
                        print('Applying to', controller)
                    # controller.set_control_mode(AuraController.AURA_CONTROL_MODE_EFFECT)
                        controller.set_rgb_data(AuraController.AURA_CONTROL_MODE_EFFECT,
                                                AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [color])

                    time.sleep(1)
                    # break
                # break
                # controller.set_effect_mode(AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'])
        except KeyboardInterrupt:
            print('done test effect')

    def get_status(self, args):
        return 'up' if self.init_done else 'initing'

    def get_list_of_devices(self):
        return self.ram_controllers + self.usb_controllers + self.remote_controllers

    def __str__(self):
        return 'AuraDaemon at ' + self.host + ':' + str(self.port)


def main():
    host = utils.get_usable_nonlocal_bind_ip()
    daemon = AuraDaemon(host=host)
    daemon.run()


if __name__ == "__main__":
    main()
