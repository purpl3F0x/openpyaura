#!/usr/bin/env python3
import socket

def get_usable_nonlocal_bind_ip():
    return ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])


red = (0xff, 0x00, 0x00)
blue = (0x00, 0x00, 0xff)
green = (0x00, 0xff, 0x00)
white = (0xff, 0xff, 0xff)
lwhite = (0x40, 0x40, 0x40)
black = (0x00, 0x00, 0x00)
