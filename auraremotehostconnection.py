#!/usr/bin/env python3
import socket
import jsonsocket
import time
import traceback


class AuraRemoteConnection(object):
    host = None
    port = None
    clientsocket = None
    stop_flag = False

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def shutdown(self):
        self.stop_flag = True

    def connect(self):
        return self._connect()

    def _connect(self):
        while not self.stop_flag:
            # print('self.stop_flag in ', self, self.stop_flag)
            try:
                # print('Trying to connect to ', self.host, self.port)
                self.clientsocket = socket.socket()
                self.clientsocket.settimeout(5)
                self.clientsocket.connect((self.host, self.port))
                print('AuraRemoteConnection connected to', self.host, self.port)
                break
            except socket.timeout as err:
                self.clientsocket.close()
                print('Failed trying to connect to ', self.host, self.port, err)
                traceback.print_exc()
                print('Sleeping 3 seconds before retrying again.')
                time.sleep(3)
        # print('self.clientsocket is now: ', self.clientsocket)
        return self.clientsocket is not None

    def _send(self, data):
        b_data, b_written = jsonsocket.write_json(self.clientsocket, data)
        # if b_data != b_written:
        #     print('Could not send all data?')
        #     print('b_data:', b_data, 'b_written', b_written)
        response = jsonsocket.read_json(self.clientsocket)
        return response

    def send_to_remote(self, datadict):
        while (not self.clientsocket) and (not self.stop_flag):
            print('Socket to remote', self.host, ':', self.port,
                  'is not set up yet! Sleeping for 1 second')
            time.sleep(1)
        resp = self._send(datadict)
        if resp['command'] == 'ack':
            if resp['status'] == 'ok':
                pass
            else:
                print(
                    'Remote device acknowledged command but returned failure.' + str(resp))
        else:
            print('Remote device did not acknowledge command. Response: ' + str(resp))
        return resp

    # def recv_from_remote(self):
    #     return self.aura_remote_connector.recv()
