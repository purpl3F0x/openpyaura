#!/usr/bin/env python3
import time
import sys
import socket

from auranetworkcontroller import AuraNetworkControllerSink
from auranetworkhostmanagerbase import AuraNetworkHostManager
from auracommandsink import AuraCommandSink
import utils

HOST = '0.0.0.0'  # Standard loopback interface address (localhost)
PORT = 8484        # Port to listen on (non-privileged ports are > 1023)


class AuraNetworkControllerHost(AuraCommandSink):
    ram_controllers = []
    usb_controllers = []
    remote_connectors = []
    init_done = False

    network_controller_sinks = []

    host = None
    port = None
    aura_network_host = None
    friendly_name = None

    def __init__(self, friendly_name = None, host='0.0.0.0', port=8484):
        if not friendly_name:
            self.friendly_name = socket.gethostname()
        print('Initing AuraNetworkControllerHost.')
        try:
            import auraramcontroller
            self.ram_controllers = auraramcontroller.detect_and_init_devices()
        except ModuleNotFoundError as err:
            print(err)
            print('Skipping aura ram controller import/setup.')

        import aurausbcontroller
        self.usb_controllers = aurausbcontroller.detect_and_init_devices()

        print('RAM controllers:', self.ram_controllers)
        print('USB controllers:', self.usb_controllers)

        print('Initing self network host.')
        self.host = host
        self.port = port

        self._init_self_network_host()
        self._init_network_controller_sinks()
        self.init_done = True
        print('Initing AuraNetworkControllerHost done.')

    def __str__(self):
        return self.friendly_name

    def __repr__(self):
        return self.friendly_name + ' - ' + str(self.ram_controllers) + ', ' + str(self.usb_controllers) + ', ' + str(self.network_controller_sinks)

    def _init_self_network_host(self):
        assert self.host is not None
        assert self.port is not None
        self.aura_network_host = AuraNetworkHostManager(self, self.host, self.port)

    def _init_network_controller_sinks(self):
        free_ports = [8485, 8486, 8487]  # generate
        for controller in self.ram_controllers + self.usb_controllers:
            n_sink = AuraNetworkControllerSink(
                controller, self.host, free_ports.pop())
            self.network_controller_sinks.append(n_sink)

    def get_status(self):
        return self.init_done

    def get_list_of_controllers(self):
        # not json serializable, see if it can be done without decoder class hacks.
        # return self.network_controller_sinks
        return [d.__json__() for d in self.network_controller_sinks]

    def run(self):
        while True:
            try:
                print('event loop hmm')
                print(self.get_list_of_controllers())
                time.sleep(3)
            except KeyboardInterrupt as err:
                print('Caught KeyboardInterrupt, triggering shutdown of network controller sinks.')
                [f.shutdown() for f in self.network_controller_sinks]
                print('From auranetworkcontrollerhost - triggering shutdown of NetworkHostManager.')
                self.aura_network_host.shutdown()
                break
        print('Shutting down networkcontrollerhost in 2 seconds.')
        time.sleep(2)
        print(self.get_list_of_controllers())
        # print('Forcing shutdown with sys.exit(1)')
        # sys.exit(1)


def main():
    print('Initing daemon.')
    host = utils.get_usable_nonlocal_bind_ip()
    daemon = AuraNetworkControllerHost(host=host)
    print('Inited daemon. Launching daemon.run()')
    daemon.run()


if __name__ == "__main__":
    main()
