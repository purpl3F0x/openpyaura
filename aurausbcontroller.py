# pyusb.py
import usb
import time
import random
import os

import auracontrollerbase
import utils

# ... etc
# blankout_msgs = [
#     0xecb0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000]

red = (0xff, 0x00, 0x00)
blue = (0x00, 0x00, 0xff)
green = (0x00, 0xff, 0x00)
white = (0xff, 0xff, 0xff)
lwhite = (0x40, 0x40, 0x40)
black = (0x00, 0x00, 0x00)


class AuraUSBController(auracontrollerbase.AuraController):

    AURA_CONTROL_MODE_EFFECT = 0x3b
    AURA_CONTROL_MODE_DIRECT = 0x40

    POSITION_SELECTORS = [0x00, 0x14, 0x28, 0x3c, 0x50, 0x64, 0x78]
    ADDR_STRIP_1 = 0x00
    ADDR_STRIP_2 = 0x01
    # modes that need just one rgb point (to investigate and fill in):
    # though it shouldn't be needed, try to match what the aura sw does
    MODES_TO_LIMIT_TO_JUST_ONE_RGB_POINT = [0x01, 0x02, 0x03]  # .. more

    command_messages_to_apply = []

    def __init__(self, device, usb_device, friendly_name):
        self.device = device
        self.friendly_name = friendly_name

        # get USB controller and set up endpoint
        # self.dev = usb.core.find(**usb_device_params)
        self.dev = usb_device

        # linux may need this
        if os.name == 'posix':
            if self.dev.is_kernel_driver_active(0):
                self.dev.detach_kernel_driver(0)
            usb.util.claim_interface(self.dev, 0)

            print("Kernel driver active?: " +
                str(self.dev.is_kernel_driver_active(0)))

        # was it found?
        if self.dev is None:
            raise ValueError('Device not found')
        # set the active configuration. With no arguments, the first
        # configuration will be the active one
        if os.name != 'posix':
            self.dev.set_configuration()

        # get an endpoint instance
        cfg = self.dev.get_active_configuration()
        intf = cfg[(0, 0)]
        self.out_ep = usb.util.find_descriptor(
            intf,
            # match the first OUT endpoint
            custom_match=lambda e: \
            usb.util.endpoint_direction(e.bEndpointAddress) == \
            usb.util.ENDPOINT_OUT)
        assert self.out_ep is not None

        # self.in_ep = usb.util.find_descriptor(
        #     intf,
        #     # match the first OUT endpoint
        #     custom_match=lambda e: \
        #     usb.util.endpoint_direction(e.bEndpointAddress) == \
        #     usb.util.ENDPOINT_IN)
        # assert self.in_ep is not None

    def __repr__(self):
        return self.friendly_name + ' - AuraUSBController at ' + str(self.dev)

    def __str__(self):
        return self.friendly_name

    def get_control_mode(self):
        # self.in_ep.read
        # return AURA_CONTROL_MODE_UNKNOWN
        raise NotImplementedError(
            'get_control_mode in USB controller not implemented.')

    def get_effect_mode(self):
        # self.in_ep.read
        raise NotImplementedError(
            'get_effect_mode in USB controller not implemented.')

    def set_effect_mode(self, mode):
        self.set_rgb_data(self.AURA_CONTROL_MODE_EFFECT, mode, [utils.black])
        # self._create_command_msgs(self.AURA_CONTROL_MODE_EFFECT, self.device, mode, [utils.black])

    def set_control_mode(self, mode):
        self.set_rgb_data(mode, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_MUSIC'], [utils.black])
        # self._create_command_msgs(mode, self.device, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_MUSIC'], [utils.black])

    # hex_msg is str, does not have leading 0x
    def write_hex_string_msg(self, ep, hex_msg):
        if len(hex_msg) != 130:
            raise Exception('hex msg len not 65: ' + str(len(hex_msg)))
        bytes_msg = bytes.fromhex(hex_msg)
        # print('sending msg', hex_msg)
        bytes_written = self.out_ep.write(bytes_msg)
        if bytes_written != 65:
            print('bytes written', bytes_written)
        return bytes_written

    # rgb_list is a list of 5 (R,B,G) tuples
    def _set_rgb_data(self, control_mode, effect_mode, rgb_list):
        print('_set_rgb_data', self, control_mode, effect_mode, rgb_list)
        cmd_msgs = self._create_command_msgs(
            control_mode, self.device, effect_mode, rgb_list)
        self.command_messages_to_apply += cmd_msgs
        self.apply_changes()

    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        print('set_rgb_data', self, control_mode, effect_mode, rgb_list)
        if control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT:
            self._set_rgb_data(self.AURA_CONTROL_MODE_EFFECT, effect_mode, rgb_list)
        elif control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            self._set_rgb_data(self.AURA_CONTROL_MODE_DIRECT, effect_mode, rgb_list)
        else:
            raise Exception('Unknown control mode: ' + str(control_mode))

    def get_rgb_data_by_mode(self, control_mode):
        raise NotImplementedError(
            'Reading from USB controller not implemented.')

    def get_rgb_data(self):
        return self.get_rgb_data_by_mode(self.get_control_mode())

    # pass rgb_tuple as (0xff,0xff,0xff) (strings) for now

    def _create_msg(self, control_mode, device, pos_selector, effect_mode, rgb_tuples):
        # one msg is 65 bytes. 5 bytes of mode info, 60 bytes of rgb data
        msg = format(0xec, '02x')
        msg += format(control_mode, '02x')
        msg += format(device, '02x')
        msg += format(pos_selector, '02x')
        msg += format(effect_mode, '02x')
        # print(msg)
        assert len(msg) == 10
        # 60 bytes of rgb
        count = 0
        while(len(rgb_tuples) < 20):
            rgb_tuples += rgb_tuples
        rgb_tuples = rgb_tuples[:20]
        try:
            assert len(rgb_tuples) == 20
        except AssertionError:
            raise Exception(rgb_tuples)
        for rgb_tuple in rgb_tuples:
            for color in rgb_tuple:
                msg += format(color, '02x')
                count += 1
            if count == 3 and effect_mode in self.MODES_TO_LIMIT_TO_JUST_ONE_RGB_POINT:
                break
        print(msg)
        assert count <= 60
        msg += ''.join('00'*(60-count))
        if len(msg) != 130:
            raise Exception('msg len is ' + str(len(msg)))
        return msg

    def _create_command_msgs(self, control_mode, device, effect_mode, rgb_tuples):
        print('_create_command_msgs', self, control_mode, effect_mode, rgb_tuples)
        if control_mode == self.AURA_CONTROL_MODE_EFFECT:
            # print('created effect mode commands.')
            return [self._create_msg(self.AURA_CONTROL_MODE_EFFECT, device, 0x00, effect_mode, rgb_tuples)]
        elif control_mode == self.AURA_CONTROL_MODE_DIRECT:
            # print('created direct mode commands.')

            msgs = []
            # self.write_hex_string_msg(self.out_ep, self._create_msg(AURA_CONTROL_MODE_EFFECT, 0x00, 0x00, 0xff, [black] * 20))
            msgs = [self._create_msg(
                self.AURA_CONTROL_MODE_EFFECT, 0x00, 0x00, 0xff, [black] * 20)]
            msgs += [self._create_msg(control_mode, device, self.POSITION_SELECTORS[i], effect_mode,
                                     rgb_tuples[self.POSITION_SELECTORS[i]:self.POSITION_SELECTORS[i+1]])
                     for i in range(5)]
            msgs += [self._create_msg(control_mode, 0x80, self.POSITION_SELECTORS[5], effect_mode,
                                     rgb_tuples[self.POSITION_SELECTORS[5]:self.POSITION_SELECTORS[6]])]
            # msgs += [self._create_msg(AURA_CONTROL_MODE_DIRECT, 0x80, 0x14, 0x10, [black] * 20)]
            msgs += [self._create_msg(self.AURA_CONTROL_MODE_EFFECT,
                                     0x01, 0x00, 0xff, [black] * 20)]
            msgs += [self._create_msg(self.AURA_CONTROL_MODE_EFFECT,
                                     0x01, 0x00, 0x14, [black] * 20)]
            msgs += [self._create_msg(self.AURA_CONTROL_MODE_EFFECT,
                                     0x81, 0x14, 0x10, [black] * 20)]
            return msgs

    def apply_changes(self):
        for msg in self.command_messages_to_apply:
            self.write_hex_string_msg(self.out_ep, msg)

        # self.write_hex_string_msg(self.out_ep, self._create_msg(AURA_CONTROL_MODE_EFFECT, 0x01, 0x00, 0xff, [black] * 20))


def test_method_effect(controller):
    while True:
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [white])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [red])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [blue])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [green])

def test_method_direct(controller):
    while True:
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [white])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [red])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [blue])
        time.sleep(2)
        controller.set_rgb_data(
            auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT, auracontrollerbase.AuraController.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [green])


def get_rand_tuple():
    return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))


def get_rand_rgb(length):
    data = []
    for i in range(length):
        # data += [((random.randint(0,255),)*3,)*20]
        # rand_t = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
        tup_r = []
        for _ in range(120):
            if i % 2 == 0:  # alternate between two colors
                tup_r.append(red)
            else:
                tup_r.append(blue)
            # tup_r.append(get_rand_tuple())
        data.append(tup_r)
    # print(data)
    return data


def test_method_arb(controller):
    rand_data = get_rand_rgb(100)
    time.sleep(2)
    # import datetime
    now = time.time()
    for data in rand_data:
        controller.set_rgb_data(controller.AURA_CONTROL_MODE_DIRECT, 0x14, data)
        time.sleep(0.04)
    diff = time.time() - now
    print('Took ', diff, 'seconds')
    # controller.set_rgb_data(AURA_CONTROL_MODE_DIRECT, 0x14, [red] * 2 + [white] * 2 + [red] * 2)
    # time.sleep(0.05)
    # controller.set_rgb_data(AURA_CONTROL_MODE_DIRECT, 0x14, [blue] * 2 + [red] * 2 + [white] * 2 + [red] * 2)
    # time.sleep(0.05)
    # controller.set_rgb_data(AURA_CONTROL_MODE_DIRECT, 0x14, [green] * 2 + [blue] * 2 + [red] * 2 + [white] * 2 + [red] * 2)
    # time.sleep(0.05)


def main():
    print('Library for interfacing with i2c aura devices. Running this as a file, provide the following for debug:')
    print('Usage: python3 auracontroller.py')

    friendly_name = 'aura device debugging'
    usb_device_params = {'idVendor': 0x0B05, 'idProduct': 0x1872}

    controller = AuraUSBController(AuraUSBController.ADDR_STRIP_1, usb_device_params, friendly_name)
    # controller.dump_aura_device()

    # test_method_arb(controller)
    test_method_effect(controller)
    print('done')


def detect_and_init_devices():
    asus_usb_device_params = {'idVendor': 0x0B05}
    asus_usb_devices = usb.core.find(**asus_usb_device_params, find_all=True)
    controllers = []
    for device in asus_usb_devices:
        controllers.append(AuraUSBController(AuraUSBController.ADDR_STRIP_1, device, 'aura usb device addr1'))
        controllers.append(AuraUSBController(AuraUSBController.ADDR_STRIP_2, device, 'aura usb device addr2'))
    return controllers


if __name__ == "__main__":
    exit(main())
