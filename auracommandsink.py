#!/usr/bin/env python3

class AuraCommandSink(object):
    def act(self, jmsg):
        func = getattr(self, jmsg['command'])
        if func:
            args = []
            kwargs = {}
            if 'args' in jmsg:
                args = jmsg['args']
            if 'kwargs' in jmsg:
                kwargs = jmsg['kwargs']
            ret = func(*args, **kwargs)
            return ret
        else:
            return {'status': 'unknown_command'}

    # def act(self, parameter_list):
    #     raise NotImplementedError
