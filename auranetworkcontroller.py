#!/usr/bin/env python3

from auracontrollerbase import AuraController
from auraremotehostconnection import AuraRemoteConnection
from auranetworkhostmanagerbase import AuraNetworkHostManager
from auracommandsink import AuraCommandSink


class AuraNetworkControllerSink(AuraCommandSink):
    controller = None
    controller_name = ''
    friendly_name = ''
    # aura_remote_connector = None
    host = None
    port = None
    aura_network_host = None

    def __init__(self, controller, host, port, controller_name=''):
        print('Initing AuraNetworkControllerSink', controller, host, port)
        self.controller = controller
        self.friendly_name = 'AuraNetworkControllerSink-' + str(controller)
        self.controller_name = self.friendly_name
        self.host = host
        self.port = port
        # self.aura_remote_connection = AuraRemoteConnection(host, port)

        self._init_self_network_host()
        self.init_done = True
        print('Init of AuraNetworkControllerSink done.')

    def _init_self_network_host(self):
        assert self.host is not None
        assert self.port is not None
        self.aura_network_host = AuraNetworkHostManager(
            self, self.host, self.port)

    # custom version of act, to send cmds to self.controller instead of ourself
    def act(self, jmsg):
        func = getattr(self.controller, jmsg['command'])
        if func:
            args = []
            kwargs = {}
            if 'args' in jmsg:
                args = jmsg['args']
            if 'kwargs' in jmsg:
                kwargs = jmsg['kwargs']
            ret = func(*args, **kwargs)
            return ret
        else:
            return {'status': 'unknown_command'}

    # def init_controller(self):
    #     return self.init_done

    # def act(self, jmsg):
    #     raise NotImplementedError

    def shutdown(self):
        self.aura_network_host.shutdown()

    def get_aura_network_host_status(self):
        if self.aura_network_host:
            return str(self.aura_network_host.get_status())
        else:
            return 'not inited yet.'

    def __str__(self):
        return self.friendly_name + ' - aura_network_host=' + str(self.aura_network_host) + ' status=' + self.get_aura_network_host_status()

    def __repr__(self):
        return self.__str__()

    def __json__(self):
        return {'controller_name': self.controller_name, 'host': self.host, 'port': self.port}

    # def __dict__(self):
    #     return {'controller_name': self.controller_name, 'friendly_name': self.friendly_name, 'host': self.host, 'port': self.port}


class AuraNetworkController(AuraController):
    controller_name = ''
    friendly_name = ''
    aura_remote_connector = None
    host = None
    port = None

    def __init__(self, controller_name, host, port):
        print('Initing AuraNetworkController', controller_name, host, port)
        self.controller_name = controller_name
        self.friendly_name = 'AuraNetworkControllerWrapper-' + controller_name
        self.host = host
        self.port = port
        self.aura_remote_connection = AuraRemoteConnection(host, port)
        print('Initing AuraNetworkController done', controller_name, host, port)

    def connect(self):
        return self.aura_remote_connection.connect()

    def shutdown(self):
        return self.aura_remote_connection.shutdown()

    def __str__(self):
        return self.friendly_name

    def __repr__(self):
        return self.friendly_name + ' at ' + str(self.host) + ':' + str(self.port)

    def send_to_remote(self, datadict):
        datadict['controller_name'] = self.controller_name
        datadict['friendly_name'] = self.friendly_name
        return self.aura_remote_connection.send_to_remote(datadict)

    def dump_aura_device(self):
        raise NotImplementedError('dump_aura_device not implemented in ', self)

    def get_rgb_data(self, mode):
        raise NotImplementedError('get_rgb_data not implemented in ', self)

    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        """
        rgb_list is list of (R,B,G) tuples of hex/int
        """
        data = {'command': 'set_rgb_data', 'args': [
            control_mode, effect_mode, rgb_list]}
        resp = self.send_to_remote(data)
        return resp['data']

    def get_effect_mode(self):
        data = {'command': 'get_effect_mode'}
        resp = self.send_to_remote(data)
        return resp['data']

    def get_control_mode(self):
        data = {'command': 'get_control_mode'}
        resp = self.send_to_remote(data)
        return resp['data']

    def set_control_mode(self, mode):
        data = {'command': 'set_control_mode', 'args': [mode]}
        resp = self.send_to_remote(data)
        return resp['data']

    def set_effect_mode(self, mode):
        data = {'command': 'set_effect_mode', 'args': [mode]}
        resp = self.send_to_remote(data)
        return resp['data']

    def apply_changes(self):
        data = {'command': 'apply_changes'}
        resp = self.send_to_remote(data)
        return resp['data']
